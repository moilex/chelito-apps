#!/bin/bash

DP_PROJECTS=("chelito-api", "chelito-web")
RSYNC=`which rsync`

for i in "${DP_PROJECTS[@]}"
do

    if [ -d "$i" ]; then

        if [ -d "../$i" ] && [ ! -z "$RSYNC" ]; then

            $RSYNC Dockerfile ../$i &&
            $RSYNC $i/supervisord.conf ../$i &&
            $RSYNC $i/entrypoint.sh ../$i && chmod +x ../$i/entrypoint.sh

          else

            echo "Dir $i does not exists or rsycn command not found"

        fi

    fi

done
