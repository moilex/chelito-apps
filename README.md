# Chelito apps #

This project set isolate enviroment on docker containers

# Getting Started

## Instalation
First we need clone repository with the command below
```bash
git clone git@bitbucket.org:moilex/chelito-apps.git
```
Stop git tracks file `env.sh` then we can do

```bash
git update-index --assume-unchanged env.sh
```

## Configuration
Add Next important variables on env.sh file to run api at least
```bash
export DATABASE_URL=postgres://${DB_USER}:${DB_PASS}@${IP}:5432/llanteradb;
export HOME_APP=${HOME_LOCATION_PROJECTS};
```

Next prompt srcipt by following
```bash
source env.sh
```

# Build

Then, you can build
```bash
docker-compose up -d --build
```

# Extra
If you want delete cache images from your host must do
```bash
docker rmi $(docker images -f "dangling=true" -q)
```
