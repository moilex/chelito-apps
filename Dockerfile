FROM node:8

RUN apt-get update && \
    apt-get install -y python curl \
    vim-common \
    openssl \
    tcpdump \
    net-tools \
    supervisor \
    --no-install-recommends && \
    rm -rf /var/lib/apt/lists/* && \
    npm install -g nodemon

# Create app directory
RUN mkdir -p /usr/src/app && \
    mkdir -p /usr/data

WORKDIR /usr/src/app

ENTRYPOINT ["./entrypoint.sh"]
